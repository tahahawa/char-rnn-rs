# char-rnn-rs

WIP char-rnn implementation in rust, inspired by:

* [https://github.com/jcjohnson/torch-rnn](https://github.com/jcjohnson/torch-rnn )
* [https://github.com/karpathy/char-rnn](https://github.com/karpathy/char-rnn)
* [https://github.com/sherjilozair/char-rnn-tensorflow](https://github.com/sherjilozair/char-rnn-tensorflow)
